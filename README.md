# Installing-Perl-5.24.1
![A](https://github.com/nu11secur1ty/Installing-Perl-5.24.1/blob/master/photo/2000px-Deluge-Logo.svg.png)
# Installation:
# Download from: [Link](http://www.mediafire.com/file/9sskx92w82gnxzz/ActivePerl-5.24.1.2402-x86_64-linux-glibc-2.15-401614.tar.gz)
```
1. tar xzvf ActivePerl-5.24.1.2402-x86_64-linux-glibc-2.15-401614.tar.gz
2. cd ActivePerl-5.24.1.2402-x86_64-linux-glibc-2.15-401614
3. bash install.sh
```
```
Recommended: installation in /opt/
```
# IMPORTANT:
```
  # If want to choose another directory for installation, you have to modify the install.sh script!
```
